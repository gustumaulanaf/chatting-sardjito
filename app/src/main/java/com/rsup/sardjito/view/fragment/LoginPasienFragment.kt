package com.rsup.sardjito.view.fragment


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import com.ic.projectabsensi.sharedPref.SharedPrefUtil

import com.rsup.sardjito.R
import com.rsup.sardjito.interfaces.pasien.LoginPasienView
import com.rsup.sardjito.model.simetris.pasien.LoginPasien
import com.rsup.sardjito.model.simetris.pasien.Pasien
import com.rsup.sardjito.presenter.pasien.LoginPasienPresenter
import com.rsup.sardjito.sharedPref.SaveDataPasien
import com.rsup.sardjito.view.activity.VerifikasiSMS
import kotlinx.android.synthetic.main.fragment_login_pasien.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class LoginPasienFragment : Fragment() , LoginPasienView {
    internal lateinit var view:View
    lateinit var loginPasienPresenter: LoginPasienPresenter
    internal lateinit var datePickerDialog: DatePickerDialog
    internal lateinit var dateFormatter: SimpleDateFormat
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view =inflater.inflate(R.layout.fragment_login_pasien, container, false)

        loginPasienPresenter = LoginPasienPresenter(this,context!!)
        view.btPickTanggal.setOnClickListener {
            showDateDialog(view.etTglLahir)
        }
        view.btLoginPasien.setOnClickListener {
            if (view.etNorm.text.isEmpty() || view.etTglLahir.text.toString().isEmpty()) {
                Toast.makeText(context, "Form Tidak Boleh Kosong", Toast.LENGTH_SHORT)
                    .show()
            } else {
                loginPasienPresenter.getLogin(view.etNorm.text.toString(), view.etTglLahir.text.toString())
            }
        }
        return  view
    }
    override fun onSuccess(loginPasien: LoginPasien){
        var saveDataPasien = SaveDataPasien()
        saveDataPasien.addDataPasien(loginPasien)
        var pasien: Pasien?
        pasien = loginPasien.pasien
        SharedPrefUtil.saveString("nama",pasien!!.nAMA!!)
        SharedPrefUtil.saveString("noTelp",pasien!!.tELP!!)
        SharedPrefUtil.saveString("noRm",view.etNorm.text.toString())
        SharedPrefUtil.saveString("ttl",view.etTglLahir.text.toString())
        SharedPrefUtil.saveString("loginSebagai","pasien")
        var nomorTelpon = pasien!!.tELP
        var indexNomor = pasien!!.tELP!!.substring(0,1)
        if (indexNomor.equals("0")) {
            indexNomor = "+62"
        }
        val nomorFull = indexNomor + nomorTelpon!!.substring(1,nomorTelpon.length)
        val intent = Intent(context, VerifikasiSMS::class.java)
        intent.putExtra("noTelp", nomorFull)
        startActivity(intent)
    }

    override fun onFailed() {
        Toast.makeText(context,"Login gagal", Toast.LENGTH_SHORT).show()
    }
    private fun showDateDialog(editText: EditText?) {
        dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val newCalendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(
            context!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                editText!!.setText(dateFormatter.format(newDate.time))
            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

}
