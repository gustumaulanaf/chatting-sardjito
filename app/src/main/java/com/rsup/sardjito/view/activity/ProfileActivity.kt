package com.rsup.sardjito.view.activity

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatDialog
import com.github.chrisbanes.photoview.PhotoView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import com.rsup.sardjito.model.firebase.Dokter
import com.rsup.sardjito.model.firebase.PasienFirebase
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_profile.*
import java.lang.Exception

class ProfileActivity : AppCompatActivity() {
    var bitmapFinal: Bitmap? = null
    var id: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        id = SharedPrefUtil.getString("id")
        title = "Profil"
        initPasien()
        fotoPasienProfile.setOnClickListener {
            val photoView = PhotoView(this)
            photoView.setImageBitmap(bitmapFinal)
            val linearLayoutManager = LinearLayout.LayoutParams(400, 400)
            photoView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            val appCompatDialog = AppCompatDialog(this,R.style.ThemeDialog)
            appCompatDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            appCompatDialog.setContentView(photoView)
            appCompatDialog.show()

        }
    }

    private fun initPasien() {
        if (SharedPrefUtil.getString("loginSebagai").equals("pasien")) {
            l2Dokter.visibility = View.GONE
            l3Dokter.visibility = View.GONE
            FirebaseDatabase.getInstance().reference.child("user/$id")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val pasien = p0.getValue(PasienFirebase::class.java)

                        tvNama.setText(pasien!!.nama)
                        tvTTLProfile.setText(SharedPrefUtil.getString("ttl"))
//                        tvNamaPasienProfile.setText(SharedPrefUtil.getString("nama"))
                        tvNormProfile.setText(SharedPrefUtil.getString("noRm"))
                        tvNoTelpProfile.setText(SharedPrefUtil.getString("noTelp"))
                        Picasso.get().load(pasien.foto).into(fotoPasienProfile)
                        Picasso.get().load(pasien.foto).into(object : Target {
                            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                            }

                            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                            }

                            override fun onBitmapLoaded(
                                bitmap: Bitmap?,
                                from: Picasso.LoadedFrom?
                            ) {
                                bitmapFinal = bitmap
                            }

                        })
                    }


                })
        }
        else{
            l2.visibility = View.GONE
            l3.visibility = View.GONE
            l4.visibility = View.GONE
            FirebaseDatabase.getInstance().reference.child("dokter/$id")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val dokter = p0.getValue(Dokter::class.java)

                        tvNama.setText(dokter!!.namaDokter)
//                        tvTTLProfile.setText(SharedPrefUtil.getString("ttl"))
//                        tvNamaPasienProfile.setText(SharedPrefUtil.getString("nama"))
//                        tvNormProfile.setText(SharedPrefUtil.getString("noRm"))
                        tvHandkey.setText(dokter.handkey)
                        tvNotelponDokter.setText(dokter!!.noTelpon)
                        Picasso.get().load(dokter.fotoDokter).into(fotoPasienProfile)
                        Picasso.get().load(dokter.fotoDokter).into(object : Target {
                            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                            }

                            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                            }

                            override fun onBitmapLoaded(
                                bitmap: Bitmap?,
                                from: Picasso.LoadedFrom?
                            ) {
                                bitmapFinal = bitmap
                            }

                        })
                    }


                })
        }
    }
}
