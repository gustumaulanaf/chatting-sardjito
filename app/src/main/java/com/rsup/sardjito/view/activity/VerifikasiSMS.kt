package com.rsup.sardjito.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.database.DatabaseReference
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import kotlinx.android.synthetic.main.activity_verifikasi_sms.*
import java.lang.Exception
import java.util.concurrent.TimeUnit

class VerifikasiSMS : AppCompatActivity() {
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var database: DatabaseReference
    var mIdVerifikasi: String? = null
    var noTelp: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verifikasi_sms)
        noTelp = intent.getStringExtra("noTelp")!!

        initFirebase()
        kirimVerifikasi(noTelp)
        gotoSetting.setOnClickListener {
            verifikasiKode(etKodeVerif.text.toString())
        }
        kirimUlang()
    }

    fun kirimUlang() {
        val delay = 1000
        var durasi = 60
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                durasi = durasi - 1
                timerKirimUlang.setText(durasi.toString())
                if (durasi == 0) {
                    tvKirimUlang.setTextColor(resources.getColor(R.color.hijautua))
                    tvKirimUlang.isEnabled = true
                    tvKirimUlang.setOnClickListener {
                        kirimVerifikasi(noTelp)
                        Toast.makeText(applicationContext, "Mengirim Ulang...", Toast.LENGTH_SHORT)
                            .show()
                        durasi = 60
                        handler.postDelayed(this, 1000)
                    }
                } else {
                    tvKirimUlang.setTextColor(resources.getColor(R.color.abu))
                    tvKirimUlang.isEnabled = false
                    handler.postDelayed(this, delay.toLong())
                }
            }

        }, delay.toLong())
    }

    private fun initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance()

    }

    private fun kirimVerifikasi(noTelp: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            noTelp, 60, TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD, mCallback
        )
    }

    private val mCallback = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(p0: PhoneAuthCredential) {
            var kode = p0!!.smsCode.toString()
            try {
                etKodeVerif.setText(kode)
                verifikasiKode(kode)
            } catch (e: Exception) {
                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        override fun onVerificationFailed(p0: FirebaseException) {
            Toast.makeText(applicationContext, p0.toString(), Toast.LENGTH_SHORT).show()
        }

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)
            super.onCodeSent(p0, p1)
            mIdVerifikasi = p0
        }

    }

    private fun verifikasiKode(kode: String) {
        try {
            val credential: PhoneAuthCredential =
                PhoneAuthProvider.getCredential(mIdVerifikasi!!, kode)
            login(credential)
        } catch (e: Exception) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    private fun login(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this, OnCompleteListener {
                if (it.isSuccessful) {
                    SharedPrefUtil.saveString("id", it.result!!.user!!.uid)
                    val intent = Intent(this, SettingPasienActivity::class.java)
                    intent.putExtra("noTelp", noTelp)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(applicationContext, "Kode Salah...", Toast.LENGTH_SHORT).show()
                }
            })
    }
}
