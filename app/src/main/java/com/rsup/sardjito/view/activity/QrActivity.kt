package com.rsup.sardjito.view.activity

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.Result
import com.rsup.sardjito.R
import com.rsup.sardjito.requestcamera.RequestCamera
import kotlinx.android.synthetic.main.activity_qr.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.nio.charset.StandardCharsets

class QrActivity : AppCompatActivity(),ZXingScannerView.ResultHandler {
    lateinit var zXingScannerView: ZXingScannerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr)
        zXingScannerView = ZXingScannerView(this)
        qrLayout.addView(zXingScannerView)
        gotoScan()

    }
    fun snackbar(peringatan:String){
        val snackbar = Snackbar.make(coordinator,peringatan, Snackbar.LENGTH_SHORT)
        snackbar.setAction("MENGERTI", View.OnClickListener {
            snackbar.dismiss()
        })
        snackbar.setActionTextColor(Color.YELLOW)
        val view: View = snackbar.view
        val textView: TextView = view.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE)
        view.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        snackbar.show()
    }
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun handleResult(rawResult: Result?) {
        val data = Base64.decode(rawResult.toString(),Base64.DEFAULT)
        val text = String(data,StandardCharsets.UTF_8)
        if(text.substring(0,8).equals("sardjito")){
//            Toast.makeText(applicationContext,text,Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ChatGrupActivity::class.java)
            intent.putExtra("namaGrup",text)
            startActivity(intent)
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        zXingScannerView.stopCamera()
    }

    override fun onStart() {
        super.onStart()
        zXingScannerView.startCamera()
    }

    override fun onResume() {
        super.onResume()
        zXingScannerView.setResultHandler(this)
        zXingScannerView.startCamera()
    }

//    private fun checkPermission(): Boolean {
//        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//        return if (result == PackageManager.PERMISSION_GRANTED) {
//            true
//        } else {
//            false
//        }
//    }

    private fun gotoScan() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (RequestCamera.checkPermission(this)) {
                zXingScannerView.startCamera()
                snackbar("Arahkan Ke Kode QR ")
            } else {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setMessage("Kamera Belum Diizinkan")
                alertDialog.setTitle("Peringatan")
                alertDialog.setPositiveButton("Beri Izin",
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        RequestCamera.requestPermission(this)
                    })
                alertDialog.setNegativeButton("Batal",
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                        finish()
                    })
                alertDialog.show()
            }
        }
    }

//    private fun requestPermission() {
//        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA)
//    }
//
//    companion object {
//        private val REQUEST_CAMERA = 1
//    }

}
