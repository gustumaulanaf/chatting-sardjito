package com.rsup.sardjito.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R

class SplashScreen : AppCompatActivity() {
    val durasi: Long = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        //  window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val handler: Handler
        handler = Handler()
        handler.postDelayed(Runnable {
          if (SharedPrefUtil.getBoolean("isLogin")){
              startActivity(Intent(this, MainActivity::class.java))
              finish()
          }
            else{
              startActivity(Intent(this, LoginActivity::class.java))
              finish()
          }
        }, durasi)
    }
}
