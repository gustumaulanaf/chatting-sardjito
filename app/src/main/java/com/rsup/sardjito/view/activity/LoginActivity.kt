package com.rsup.sardjito.view.activity

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.rsup.sardjito.R
import com.rsup.sardjito.view.fragment.LoginDokterFragment
import com.rsup.sardjito.view.fragment.LoginPasienFragment
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity() {

    val REGEX_NO_HP: Pattern =
        Pattern.compile("\\(?(?:\\+62|0)(?:\\d{2,3})?\\)?[ .-]?\\d{2,4}[ .-]?\\d{2,4}[ .-]?\\d{2,4}")

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getFragment(LoginPasienFragment())
        viewPasien.visibility = View.VISIBLE
        viewDokter.visibility = View.INVISIBLE
        tvSebagaiDokter.setOnClickListener {
            getFragment(LoginDokterFragment())
            tvSebagaiDokter.setTextSize(20f)
            tvSebagaiDokter.setTextColor(ContextCompat.getColor(applicationContext,R.color.hijautua))
            tvSebagaiPasien.setTextColor(ContextCompat.getColor(applicationContext,R.color.hitam))
            tvSebagaiPasien.setTextSize(12f)
            viewDokter.visibility = View.VISIBLE
            viewPasien.visibility = View.INVISIBLE
        }
        tvSebagaiPasien.setOnClickListener {
            getFragment(LoginPasienFragment())
            tvSebagaiPasien.setTextSize(20f)
            tvSebagaiPasien.setTextColor(ContextCompat.getColor(applicationContext,R.color.hijautua))
            tvSebagaiDokter.setTextSize(12f)
            tvSebagaiDokter.setTextColor(ContextCompat.getColor(applicationContext,R.color.hitam))
            viewPasien.visibility = View.VISIBLE
            viewDokter.visibility = View.INVISIBLE
        }
    }

    fun getFragment(fragment: Fragment):Boolean{
        supportFragmentManager.beginTransaction().replace(R.id.frameLogin,fragment).addToBackStack(null).commit()
        return true
    }




    fun regexNoTelp(noTelp: String): Boolean {
        var matcher: Matcher = REGEX_NO_HP.matcher(noTelp)
        return matcher.find()
    }
}
