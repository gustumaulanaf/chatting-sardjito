package com.rsup.sardjito.view.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.core.widget.addTextChangedListener
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import com.rsup.sardjito.model.firebase.Chat
import com.rsup.sardjito.model.firebase.Dokter
import com.rsup.sardjito.model.firebase.Grup
import com.rsup.sardjito.model.firebase.PasienFirebase
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_chat_grup.*
import java.text.SimpleDateFormat
import java.util.*

class ChatGrupActivity : AppCompatActivity() {
    lateinit var databaseReference: FirebaseDatabase
    lateinit var roomGrup:DatabaseReference
    var keyChat = ""
    var keyGrup = ""
    var namaGrup = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_grup)
        keyGrup = intent.getStringExtra("keyGrup")
        namaGrup = intent.getStringExtra("namaGrup")
        setTitle(namaGrup)
        if (keyGrup == null) {
            Toast.makeText(applicationContext, "null", Toast.LENGTH_LONG).show()
            finish()
        } else {
            FirebaseDatabase.getInstance().reference.child("Grup/" + keyGrup)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                    override fun onDataChange(p0: DataSnapshot) {

                        val grup = p0.getValue(Grup::class.java)
                      if (grup==null){
                          val grup  = Grup()
                          grup.nama = namaGrup
                          FirebaseDatabase.getInstance().getReference().child("Grup/$keyGrup").setValue(grup).addOnCompleteListener(object :OnCompleteListener<Void>{
                              override fun onComplete(p0: Task<Void>) {
                              }

                          })
                      }
                    }

                })

        }
        initChat()
        editTextPesan.addTextChangedListener {
            jumlahtext.setText("${it!!.length}/100")
            if (it.length>100){

            }
        }
        buttonSend.setOnClickListener {
            if (!editTextPesan.text.isEmpty()){
                val locale  = Locale("in","ID")
                val formatTgl = SimpleDateFormat("EEEE,yyyy-MM-dd HH.mm",locale)
                val tanggal = formatTgl.format(Calendar.getInstance().time)
                val map = java.util.HashMap<String, Any>()
                keyChat = roomGrup.push().key!!
                roomGrup.updateChildren(map)
                val message_root = roomGrup.child(keyChat)
                val map2 = java.util.HashMap<String, Any>()
                map2.put("pengirim",SharedPrefUtil.getString("id")!!)
                map2.put("pesan",editTextPesan.text.toString())
                map2.put("waktu",tanggal.toString())
                map2.put("sebagai",SharedPrefUtil.getString("loginSebagai")!!)
                message_root.updateChildren(map2)
            }

        }
        roomGrup.addChildEventListener(object : ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
//                Toast.makeText(applicationContext,p0.toString(),Toast.LENGTH_LONG).show()
                val chat = p0.getValue(Chat::class.java)
            //    Toast.makeText(applicationContext,chat!!.pesan,Toast.LENGTH_SHORT).show()
                appendKeTampilan(chat!!.pengirim,chat.pesan,chat.waktu ,chat.sebagai)
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }


        })

    }

    private fun appendKeTampilan(pengirim: String, pesan: String, waktu: String,sebagai:String) {
       if (sebagai.equals("pasien")){
       if (SharedPrefUtil.getString("loginSebagai").equals("dokter")){
           val inflater = LayoutInflater.from(this)
           val v = inflater.inflate(R.layout.chat_bubble_pasien,null)
           val namaPasien = v.findViewById(R.id.namaPasien) as TextView
           val imageView = v.findViewById(R.id.imgSender) as CircleImageView
           getProfile(pengirim,namaPasien)
           imageView.setImageResource(R.drawable.ic_send_me)
           val tvPesan = v.findViewById(R.id.textChatPasien) as TextView
           val time = v.findViewById(R.id.waktuChat) as TextView
           namaPasien.setText("$pengirim(Pasien)")
           tvPesan.setText(pesan)
           time.setText(waktu)
           layout1.addView(v)
       }
           else{
           if (pengirim.equals(SharedPrefUtil.getString("id"))){
               val inflater = LayoutInflater.from(this)
               val v = inflater.inflate(R.layout.chat_bubble_pasien,null)
               val namaPasien = v.findViewById(R.id.namaPasien) as TextView
               val imageView = v.findViewById(R.id.imgSender) as CircleImageView
//               getProfile(pengirim,namaPasien)
               imageView.setImageResource(R.drawable.ic_send_me)
               val tvPesan = v.findViewById(R.id.textChatPasien) as TextView
               val time = v.findViewById(R.id.waktuChat) as TextView
               namaPasien.setText("Anda(Pasien)")
               tvPesan.setText(pesan)
               time.setText(waktu)
               layout1.addView(v)
           }
           else {
               val inflater = LayoutInflater.from(this)
               val v = inflater.inflate(R.layout.chat_bubble_pasien_lain,null)
               val namaPasien = v.findViewById(R.id.namaPasien) as TextView
               val imageView = v.findViewById(R.id.imgSender) as CircleImageView
               getProfileLain(pengirim,namaPasien)
               imageView.setImageResource(R.drawable.ic_send_other)
               val tvPesan = v.findViewById(R.id.textChatPasien) as TextView
               val time = v.findViewById(R.id.waktuChat) as TextView
               namaPasien.setText("$pengirim+(Pasien)")
               tvPesan.setText(pesan)
               time.setText(waktu)
               layout1.addView(v)
           }
       }
       }
        else {
           if (pengirim.equals(SharedPrefUtil.getString("id"))){
               val inflater = LayoutInflater.from(this)
               val v = inflater.inflate(R.layout.chat_bubble_dokter,null)
               val namaDokter = v.findViewById(R.id.namaDokterChat) as TextView
//               val imageView = v.findViewById(R.id.fotoDokterChat) as CircleImageView
//               getProfileDokter(pengirim,namaDokter)
               val tvPesan = v.findViewById(R.id.textChatDokter) as TextView
               val time = v.findViewById(R.id.waktuChatDokter) as TextView
               namaDokter.setText("Anda(Dokter)")
               tvPesan.setText(pesan)
               time.setText(waktu)
               layout1.addView(v)
           }
           else if (!pengirim.equals(SharedPrefUtil.getString("id"))){
               val inflater = LayoutInflater.from(this)
               val v = inflater.inflate(R.layout.chat_bubble_dokter,null)
               val namaDokter = v.findViewById(R.id.namaDokterChat) as TextView
//               val imageView = v.findViewById(R.id.fotoDokterChat) as CircleImageView
               getProfileDokter(pengirim,namaDokter)
               val tvPesan = v.findViewById(R.id.textChatDokter) as TextView
               val time = v.findViewById(R.id.waktuChatDokter) as TextView
               tvPesan.setText(pesan)
               time.setText(waktu)
               layout1.addView(v)
           }
        }
        nestedScrollViewChatGrup.post(
            object :Runnable{
                override fun run() {
                    nestedScrollViewChatGrup.fullScroll(View.FOCUS_DOWN)
                }

            }
        )
        editTextPesan.setText("")
    }

    private fun getProfileDokter(pengirim: String, namaDokter: TextView) {
        FirebaseDatabase.getInstance().reference.child("dokter/$pengirim").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }
            override fun onDataChange(p0: DataSnapshot) {
            val nama = p0.child("namaDokter").value
               namaDokter.setText("$nama(Dokter)")

            }

        })
    }

    private fun initChat() {
        FirebaseAuth.getInstance()
        databaseReference = FirebaseDatabase.getInstance()
        roomGrup = FirebaseDatabase.getInstance().reference.child("Grup/$keyGrup/chat")
    }
    fun getProfile(pengirim: String ,nama:TextView){
        FirebaseDatabase.getInstance().reference.child("user/$pengirim").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                val pasien = p0.getValue(PasienFirebase::class.java)
                nama.setText("${pasien!!.nama}(Pasien)")
            }

        })
    }
    fun getProfileLain(pengirim: String,nama: TextView){
        FirebaseDatabase.getInstance().getReference().child("user/$pengirim").addChildEventListener(object :ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }
}
