package com.rsup.sardjito.view.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.format.Time
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.gson.Gson
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import com.rsup.sardjito.imageutil.ImageRotator
import com.rsup.sardjito.model.firebase.PasienFirebase
import com.rsup.sardjito.model.simetris.pasien.KunjunganItem
import com.rsup.sardjito.requestcamera.RequestCamera
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_setting_pasien.*
import kotlinx.android.synthetic.main.item_ambil_foto.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.lang.Exception

class SettingPasienActivity : AppCompatActivity() {
    var noTelp: String = ""
    val REQUEST_GALERI = 0
    val REQUEST_KAMERA = 1
    val REQUEST_WRITE_STORAGE_REQUEST_CODE = 2
    var mHighQualityImageUri: Uri? = null
    lateinit var alertDialog: AlertDialog
    lateinit var userDb: DatabaseReference
    lateinit var grupListDb: DatabaseReference
    var id = ""
    lateinit var spotsDialog: SpotsDialog
    var bitmapFinal: Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_pasien)
//        val saveDataPasien= SaveDataPasien()
//        val kunjunganItem = saveDataPasien.getDataPasien().kunjungan
//        Toast.makeText(applicationContext,kunjunganItem!!.get(0)!!.pOLIKLINIK!!,Toast.LENGTH_SHORT).show()
//


        spotsDialog = SpotsDialog(this, R.style.CustomProgressDialog)
        FirebaseAuth.getInstance()
        FirebaseDatabase.getInstance()
        requestAppPermissions()
        RequestCamera.requestPermission(this)
        noTelp = intent.getStringExtra("noTelp")!!
        id = SharedPrefUtil.getString("id")!!
        etNamaPasien.setText(SharedPrefUtil.getString("nama")!!)
        initPasien()
        btGantiFoto.setOnClickListener {
            val alertDialogBuilder = AlertDialog.Builder(this)
            var view: View
            view = layoutInflater.inflate(R.layout.item_ambil_foto, null)
            alertDialogBuilder.setView(view)
            view.layoutGaleri.setOnClickListener {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_GALERI)
            }
            view.layoutKamera.setOnClickListener {
                if (RequestCamera.checkPermission(applicationContext)) {
                    intentKamera()
                } else {
                    RequestCamera.requestPermission(this)
                }
            }
            alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        }
        etNoTelp.setText(noTelp)

        gotoMainMenu.setOnClickListener {
            spotsDialog.show()
            if (bitmapFinal == null) {
                Toast.makeText(applicationContext, "Harap Upload Foto Anda", Toast.LENGTH_SHORT)
                    .show()
                spotsDialog.dismiss()
            } else {
                if (etNamaPasien.text.isEmpty()) {
                    Toast.makeText(
                        applicationContext,
                        "Harap Masukkan Nama Anda",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    uploadFoto(bitmapFinal!!)
                }
            }
        }

    }

    private fun initPasien() {
        spotsDialog.show()
        FirebaseDatabase.getInstance().reference.child("user/$id")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {

                    val pasien = p0.getValue(PasienFirebase::class.java)
                    if (pasien==null) {
                        Toast.makeText(applicationContext,"Selamat Datang",Toast.LENGTH_SHORT).show()
                        etNamaPasien.setText(SharedPrefUtil.getString("nama")!!)
                        etNoTelp.setText(noTelp)
                    } else {
                        etNamaPasien.setText(pasien.nama)
                        Picasso.get().load(pasien.foto).into(fotoProfilePasien)
                        Picasso.get().load(pasien.foto).into(object : Target {
                            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                            }

                            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                            }

                            override fun onBitmapLoaded(
                                bitmap: Bitmap?,
                                from: Picasso.LoadedFrom?
                            ) {
                                bitmapFinal = bitmap
                            }

                        })
                    }
                    spotsDialog.dismiss()
                }

            })
    }

    private fun saveDataPasien(nama: String, noTelpon: String, foto: String) {
        var pasien = PasienFirebase()
        pasien.foto = foto
        pasien.nama = nama
        pasien.noTelp = noTelpon
        userDb = FirebaseDatabase.getInstance().reference.child("user/" + id)
        userDb.setValue(pasien).addOnCompleteListener(object : OnCompleteListener<Void> {
            override fun onComplete(p0: Task<Void>) {
                if (p0.isSuccessful) {
                    SharedPrefUtil.saveString("noTelp", noTelp)
                    SharedPrefUtil.saveString("loginSebagai","pasien")
                    SharedPrefUtil.saveBoolean("isLogin",true)
                    startActivity(Intent(this@SettingPasienActivity, MainActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(applicationContext, "User Gagal Ditambahkan", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
        grupListDb = FirebaseDatabase.getInstance().reference.child("listgrup")
        userDb.setValue(pasien)

    }

    internal fun intentKamera() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        mHighQualityImageUri = generateTimeStampPhotoFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri)
        startActivityForResult(intent, REQUEST_KAMERA)
    }

    private fun generateTimeStampPhotoFileUri(): Uri? {
        var photoFileUri: Uri? = null
        val code = applicationContext.packageManager.checkPermission(
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            applicationContext.packageName
        )
        if (code == PackageManager.PERMISSION_GRANTED) {
            // todo create directory
            val outputDir = getPhotoDirectory()
            if (outputDir != null) {
                val t = Time()
                t.setToNow()
                val photoFile = File(outputDir, System.currentTimeMillis().toString() + ".jpg")
                photoFileUri = Uri.fromFile(photoFile)
                return photoFileUri

            }

        } else {
            Toast.makeText(applicationContext, "Gagal", Toast.LENGTH_SHORT).show()
        }
        return photoFileUri
    }

    private fun getPhotoDirectory(): File? {
        var outputDir: File? = null
        val externalStorageStagte = Environment.getExternalStorageState()
        if (externalStorageStagte == Environment.MEDIA_MOUNTED) {
            val photoDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            outputDir = File(photoDir, getString(R.string.app_name))
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(
                        this,
                        "Gagal Membuat Direktori " + outputDir.absolutePath,
                        Toast.LENGTH_SHORT
                    ).show()
                    outputDir = null
                }
        }
        return outputDir
    }

    private fun requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return
        }

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), REQUEST_WRITE_STORAGE_REQUEST_CODE
        )
    }

    private fun hasReadPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            baseContext,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun hasWritePermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            baseContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GALERI && resultCode == Activity.RESULT_OK) {
            if (data!!.data != null) {
                val uri = data.data
                var inputStream: InputStream? = null
                try {
                    inputStream = this.contentResolver.openInputStream(uri!!)
                    val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                    val rotateBitmap: Bitmap =
                        ImageRotator.rotateImageIfRequired(bitmap, applicationContext, uri)
                    bitmapFinal = rotateBitmap
                    fotoProfilePasien.setImageBitmap(rotateBitmap)
                    alertDialog.dismiss()
                } catch (e: FileNotFoundException) {

                }
            }
        } else if (requestCode == REQUEST_KAMERA && resultCode == Activity.RESULT_OK) {

            var inputStream: InputStream? = null
            try {
                inputStream = this.contentResolver.openInputStream(mHighQualityImageUri!!)
                val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                val rotateBitmap: Bitmap = ImageRotator.rotateImageIfRequired(
                    bitmap,
                    applicationContext,
                    mHighQualityImageUri!!
                )
                bitmapFinal = rotateBitmap
                fotoProfilePasien.setImageBitmap(rotateBitmap)
                alertDialog.dismiss()
            } catch (e: FileNotFoundException) {

            }
        }
    }

    internal fun uploadFoto(bitmap: Bitmap) {
        val firebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
        var storageReference: StorageReference = firebaseStorage.getReference(id)
        var imageRef: StorageReference =
            storageReference.child("images/" + id + ".jpg")
        val bao = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao)
        val data = bao.toByteArray();
        val uploadTask: UploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            Toast.makeText(applicationContext, it.toString(), Toast.LENGTH_SHORT).show()
        }.addOnSuccessListener {
            imageRef.downloadUrl.addOnSuccessListener {
                val url: Uri = it
                saveDataPasien(etNamaPasien.text.toString(), noTelp, url.toString())
            }
        }
    }
}
