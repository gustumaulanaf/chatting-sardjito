package com.rsup.sardjito.view.activity

import RecyclerItemClickListener
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.firebase.database.*
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import com.rsup.sardjito.adapter.MainAdapter
import com.rsup.sardjito.interfaces.dokter.LoginDokterView
import com.rsup.sardjito.interfaces.pasien.LoginPasienView
import com.rsup.sardjito.model.firebase.Dokter
import com.rsup.sardjito.model.firebase.Grup
import com.rsup.sardjito.model.firebase.PasienFirebase
import com.rsup.sardjito.model.simetris.dokter.LoginDokter
import com.rsup.sardjito.model.simetris.pasien.LoginPasien
import com.rsup.sardjito.presenter.dokter.LoginDokterPresenter
import com.rsup.sardjito.presenter.pasien.LoginPasienPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_main_menu.*
import kotlinx.android.synthetic.main.nav_header_main_menu.view.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener ,LoginDokterView,LoginPasienView{

    internal lateinit var mBundelRecyclerViewState:Bundle
    final var  KEY_RECYCLER_STATE = "recycler_state"
    lateinit var drawer: DrawerLayout
    lateinit var toggle: ActionBarDrawerToggle
    lateinit var mainAdapter: MainAdapter
    lateinit var userDb: DatabaseReference
    lateinit var navigationView: NavigationView
    lateinit var pasienPresenter: LoginPasienPresenter
    lateinit var dokterPresenter: LoginDokterPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        val toolbar: androidx.appcompat.widget.Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawer = findViewById(R.id.drawer_layout)
        toggle = ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        initUser()
        //initFirebase()
//        fab.setOnClickListener {
//            startActivity(Intent(this, QrActivity::class.java))
//        }
    }

    private fun initUser() {
        var id :String ? = null
        if (SharedPrefUtil.getString("loginSebagai").equals("pasien")){
            id   = "user/${SharedPrefUtil.getString("id")}"
            pasienPresenter = LoginPasienPresenter(this,applicationContext)
            pasienPresenter.getLogin(SharedPrefUtil.getString("noRm")!!,  SharedPrefUtil.getString("ttl")!!)

        }
        else{
             id   = "dokter/${SharedPrefUtil.getString("id")}"
            dokterPresenter = LoginDokterPresenter(this,applicationContext)
            dokterPresenter.login(SharedPrefUtil.getString("handkey")!!)
        }
        userDb = FirebaseDatabase.getInstance()
            .reference.child(id!!)
        userDb.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (SharedPrefUtil.getString("loginSebagai").equals("pasien")){
                    val pasien = p0.getValue(PasienFirebase::class.java)
                    val headerNav:View = navigationView.getHeaderView(0)
                    Picasso.get().load(pasien!!.foto).into(headerNav.imageViewUserNav)
                    headerNav.namaUserNav.setText(pasien!!.nama)
                    headerNav.nomorTelephoneNav.setText(pasien.noTelp)
                    headerNav.layoutHeaderNav.setOnClickListener {
                        val intent = Intent(this@MainActivity,
                            ProfileActivity::class.java)
                        startActivity(intent)
                    }
                }
                else{
                    val dokter = p0.getValue(Dokter::class.java)
                    val headerNav:View = navigationView.getHeaderView(0)
                    Picasso.get().load(dokter!!.fotoDokter).into(headerNav.imageViewUserNav)
                    headerNav.namaUserNav.setText(dokter!!.namaDokter)
                    headerNav.nomorTelephoneNav.setText(dokter.noTelpon)
                    headerNav.layoutHeaderNav.setOnClickListener {
                        val intent = Intent(this@MainActivity,
                            ProfileActivity::class.java)
                        startActivity(intent)
                    }
                }
            }

        })
    }

//    private fun initFirebase() {
//        database = FirebaseDatabase.getInstance()
//        database.getReference().child("Grup").addValueEventListener(object : ValueEventListener {
//            override fun onCancelled(p0: DatabaseError) {
//                Toast.makeText(applicationContext, p0.toString(), Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onDataChange(p0: DataSnapshot) {
//                listGrup.clear()
//                for (child: DataSnapshot in p0.children) {
//                    var grup: Grup? = child.getValue(Grup::class.java)
//                    listGrup!!.add(grup!!)
//                    listkey.add(child.key!!)
//                    mainAdapter = MainAdapter(listGrup!!,this@MainActivity,listkey)
//                    val linearLayout = LinearLayoutManager(this@MainActivity)
//                    rvListGrup.setHasFixedSize(true)
//                    rvListGrup.layoutManager = linearLayout
//                    rvListGrup.adapter = mainAdapter
//                }
//            }
//
//        })
//    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.btLogout -> Toast.makeText(applicationContext, "Logout", Toast.LENGTH_SHORT).show()
            R.id.btInfo -> Toast.makeText(applicationContext, "Info", Toast.LENGTH_SHORT).show()
        }
        drawer.closeDrawer(GravityCompat.START)

        return true
    }

    //Dokter
    override fun _onSuccess(loginDokter: LoginDokter) {
        if (SharedPrefUtil.getString("loginSebagai").equals("dokter")){
            var listGrup :ArrayList<String> = ArrayList()
            var listKeyGrup :ArrayList<String> = ArrayList()
            for (i in loginDokter.pasien!!.indices){
                listGrup.add(loginDokter.pasien!!.get(i)!!.pOLI!!)
                listKeyGrup.add(loginDokter.pasien!!.get(i)!!.kODEPOLI!!)
            }
            mainAdapter = MainAdapter(listGrup,applicationContext,listKeyGrup)
            val linearLayout = LinearLayoutManager(applicationContext)
            rvListGrup.layoutManager = linearLayout
            rvListGrup.setHasFixedSize(true)
            rvListGrup.adapter = mainAdapter
            mainAdapter.notifyDataSetChanged()
            rvListGrup.addOnItemTouchListener(RecyclerItemClickListener(applicationContext,rvListGrup,object :RecyclerItemClickListener.OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {
                    val intent = Intent(this@MainActivity, ChatGrupActivity::class.java)
                    intent.putExtra("keyGrup",listKeyGrup.get(position))
                    intent.putExtra("namaGrup",listGrup.get(position))
                    startActivity(intent)
                }

                override fun onLongItemClick(view: View?, position: Int) {
                }

            }))

        }
    }

    override fun _onFailed() {
    }
//Pasien
    override fun onSuccess(loginPasien: LoginPasien) {
        if (SharedPrefUtil.getString("loginSebagai").equals("pasien")){
            var listGrup :ArrayList<String> = ArrayList()
            var listKeyGrup :ArrayList<String> = ArrayList()
            for (i in loginPasien.kunjungan!!.indices){
                listGrup.add(loginPasien.kunjungan!!.get(i)!!.pOLIKLINIK!!)
                listKeyGrup.add(loginPasien.kunjungan!!.get(i)!!.kODEPOLIKLINIK!!)
            }
            mainAdapter = MainAdapter(listGrup,applicationContext,listKeyGrup)
            val linearLayout = LinearLayoutManager(applicationContext)
            rvListGrup.layoutManager = linearLayout
            rvListGrup.setHasFixedSize(true)
            rvListGrup.adapter = mainAdapter
            mainAdapter.notifyDataSetChanged()

            rvListGrup.addOnItemTouchListener(RecyclerItemClickListener(applicationContext,rvListGrup,object :RecyclerItemClickListener.OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {
                    val intent = Intent(this@MainActivity, ChatGrupActivity::class.java)
                    intent.putExtra("keyGrup",listKeyGrup.get(position))
                    intent.putExtra("namaGrup",listGrup.get(position))
                    startActivity(intent)
                }

                override fun onLongItemClick(view: View?, position: Int) {
                }

            }))

        }
    }

    override fun onFailed() {
    }


}
