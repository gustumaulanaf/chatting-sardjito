package com.rsup.sardjito.view.fragment


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ic.projectabsensi.sharedPref.SharedPrefUtil

import com.rsup.sardjito.R
import com.rsup.sardjito.interfaces.dokter.LoginDokterView
import com.rsup.sardjito.model.simetris.dokter.LoginDokter
import com.rsup.sardjito.presenter.dokter.LoginDokterPresenter
import com.rsup.sardjito.sharedPref.ModelPreferences
import com.rsup.sardjito.view.activity.SettingDokterActivity
import com.rsup.sardjito.view.activity.SettingPasienActivity
import kotlinx.android.synthetic.main.fragment_login_dokter.*
import kotlinx.android.synthetic.main.fragment_login_dokter.view.*

/**
 * A simple [Fragment] subclass.
 */
class LoginDokterFragment : Fragment() , LoginDokterView {
    internal lateinit var view:View
    internal lateinit var modelPreferences: ModelPreferences
    lateinit var loginDokterPresenter: LoginDokterPresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initPresenter()
        view =  inflater.inflate(R.layout.fragment_login_dokter, container, false)
        // Inflate the layout for this fragment
        view.btLoginDokter.setOnClickListener {
            loginDokterPresenter.login(etHandkey.text.toString())
        }
        return view
    }
  fun  initPresenter(){
       loginDokterPresenter = LoginDokterPresenter(this,context!!)

    }
    override fun _onSuccess(loginDokter: LoginDokter) {
        SharedPrefUtil.saveString("nama",loginDokter.pasien!!.get(0)!!.nAMA!!)
        SharedPrefUtil.saveString("id",etHandkey.text.toString())
        val intent = Intent(context!!,SettingDokterActivity::class.java)
        intent.putExtra("nama",loginDokter.pasien.get(0)!!.nAMA!!)
        startActivity(intent)

    }

    override fun _onFailed() {
        Toast.makeText(context!!,"Login Gagal",Toast.LENGTH_SHORT).show()
    }

}
