package com.rsup.sardjito.view.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.format.Time
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.drawToBitmap
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.R
import com.rsup.sardjito.imageutil.ImageRotator
import com.rsup.sardjito.model.firebase.Dokter
import com.rsup.sardjito.requestcamera.RequestCamera
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_setting_dokter.*
import kotlinx.android.synthetic.main.activity_setting_dokter.btGantiFoto
import kotlinx.android.synthetic.main.activity_setting_dokter.etNoTelp
import kotlinx.android.synthetic.main.activity_setting_pasien.*
import kotlinx.android.synthetic.main.item_ambil_foto.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.lang.Exception

class SettingDokterActivity : AppCompatActivity() {
    val REQUEST_GALERI = 0
    val REQUEST_KAMERA = 1
    val REQUEST_WRITE_STORAGE_REQUEST_CODE = 2
    var mHighQualityImageUri: Uri? = null
    var bitmapFinal: Bitmap? = null
    lateinit var alertDialog: AlertDialog

    lateinit var dbDokter:DatabaseReference
    var handkey : String ? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_dokter)
        val nama = intent.getStringExtra("nama")
        handkey = SharedPrefUtil.getString("id")
        etNamaDokter.setText(nama)
        fotoProfileDokter.isDrawingCacheEnabled = true
        dbDokter = FirebaseDatabase.getInstance().reference.child("dokter/$handkey")
        getProfile()
        btDone.setOnClickListener {
           if (bitmapFinal!=null){

               uploadFoto(bitmapFinal!!)
           }
            else{
               Toast.makeText(applicationContext,"Maaf Harap Upload Ulang Foto Anda",Toast.LENGTH_SHORT).show()
           }
        }
        btGantiFoto.setOnClickListener {

            val alertDialogBuilder = AlertDialog.Builder(this)
            var view: View
            view = layoutInflater.inflate(R.layout.item_ambil_foto, null)
            alertDialogBuilder.setView(view)
            view.layoutGaleri.setOnClickListener {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_GALERI)
            }
            view.layoutKamera.setOnClickListener {
                if (RequestCamera.checkPermission(applicationContext)) {
                    intentKamera()
                } else {
                    RequestCamera.requestPermission(this)
                }
            }
            alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        }
    }

    private fun pushProfile(nama:String,noTelpon:String,uri:String) {
        val dokter = Dokter()
        dokter.fotoDokter = uri
        dokter.noTelpon = noTelpon
        dokter.namaDokter = nama
        dokter.handkey = handkey!!
        dbDokter.setValue(dokter).addOnCompleteListener(object :OnCompleteListener<Void>{
            override fun onComplete(p0: Task<Void>) {
                SharedPrefUtil.saveString("loginSebagai","dokter")
                SharedPrefUtil.saveBoolean("isLogin",true)
                startActivity(Intent(applicationContext,MainActivity::class.java))
                finish()
            }

        })

    }

    private fun getProfile() {

        dbDokter.addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                val dokter = p0.getValue(Dokter::class.java)
                if (dokter==null){
                    Toast.makeText(applicationContext,"Selamat Datang",Toast.LENGTH_SHORT).show()
                }
                else{
                    etNamaDokter.setText(dokter.namaDokter)
                    etNoTelp.setText(dokter.noTelpon)
                    Picasso.get().load(dokter.fotoDokter).into(fotoProfileDokter)
                    Picasso.get().load(dokter.fotoDokter).into(object : Target {
                        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        }

                        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        }

                        override fun onBitmapLoaded(
                            bitmap: Bitmap?,
                            from: Picasso.LoadedFrom?
                        ) {
                            bitmapFinal = bitmap
                        }

                    })
                }
            }

        })

    }
    internal fun intentKamera() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        mHighQualityImageUri = generateTimeStampPhotoFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri)
        startActivityForResult(intent, REQUEST_KAMERA)
    }

    private fun generateTimeStampPhotoFileUri(): Uri? {
        var photoFileUri: Uri? = null
        val code = applicationContext.packageManager.checkPermission(
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            applicationContext.packageName
        )
        if (code == PackageManager.PERMISSION_GRANTED) {
            // todo create directory
            val outputDir = getPhotoDirectory()
            if (outputDir != null) {
                val t = Time()
                t.setToNow()
                val photoFile = File(outputDir, System.currentTimeMillis().toString() + ".jpg")
                photoFileUri = Uri.fromFile(photoFile)
                return photoFileUri

            }

        } else {
            Toast.makeText(applicationContext, "Gagal", Toast.LENGTH_SHORT).show()
        }
        return photoFileUri
    }

    private fun getPhotoDirectory(): File? {
        var outputDir: File? = null
        val externalStorageStagte = Environment.getExternalStorageState()
        if (externalStorageStagte == Environment.MEDIA_MOUNTED) {
            val photoDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            outputDir = File(photoDir, getString(R.string.app_name))
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(
                        this,
                        "Gagal Membuat Direktori " + outputDir.absolutePath,
                        Toast.LENGTH_SHORT
                    ).show()
                    outputDir = null
                }
        }
        return outputDir
    }

    private fun requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return
        }

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), REQUEST_WRITE_STORAGE_REQUEST_CODE
        )
    }

    private fun hasReadPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            baseContext,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun hasWritePermissions(): Boolean {
        return ContextCompat.checkSelfPermission(
            baseContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GALERI && resultCode == Activity.RESULT_OK) {
            if (data!!.data != null) {
                val uri = data.data
                var inputStream: InputStream? = null
                try {
                    inputStream = this.contentResolver.openInputStream(uri!!)
                    val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                    val rotateBitmap: Bitmap =
                        ImageRotator.rotateImageIfRequired(bitmap, applicationContext, uri)
//                    bitmapFinal = rotateBitmap
                    fotoProfileDokter.setImageBitmap(rotateBitmap)
                    val bitmapDrawable = fotoProfileDokter.drawable as BitmapDrawable
                    bitmapFinal = bitmapDrawable.bitmap
                    alertDialog.dismiss()
                } catch (e: FileNotFoundException) {

                }
            }
        } else if (requestCode == REQUEST_KAMERA && resultCode == Activity.RESULT_OK) {

            var inputStream: InputStream? = null
            try {
                inputStream = this.contentResolver.openInputStream(mHighQualityImageUri!!)
                val bitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                val rotateBitmap: Bitmap = ImageRotator.rotateImageIfRequired(
                    bitmap,
                    applicationContext,
                    mHighQualityImageUri!!
                )
                bitmapFinal = rotateBitmap
                fotoProfileDokter.setImageBitmap(rotateBitmap)
                alertDialog.dismiss()
            } catch (e: FileNotFoundException) {

            }
        }
    }

    internal fun uploadFoto(bitmap: Bitmap) {
        val firebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
        var storageReference: StorageReference = firebaseStorage.getReference(handkey!!)
        var imageRef: StorageReference =
            storageReference.child("images/" + handkey + ".jpg")
        val bao = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao)
        val data = bao.toByteArray();
        val uploadTask: UploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            Toast.makeText(applicationContext, it.toString(), Toast.LENGTH_SHORT).show()
        }.addOnSuccessListener {
            imageRef.downloadUrl.addOnSuccessListener {
                val url: Uri = it
                pushProfile(etNamaDokter.text.toString(), etNoTelp.text.toString(), url.toString())
            }
        }
    }
}
