package com.rsup.sardjito.interfaces.pasien

import android.view.View
import com.rsup.sardjito.model.simetris.pasien.LoginPasien

interface LoginPasienView {
    fun onSuccess(loginPasien: LoginPasien)
    fun onFailed()
}