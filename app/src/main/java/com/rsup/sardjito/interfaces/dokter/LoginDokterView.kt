package com.rsup.sardjito.interfaces.dokter

import android.view.View
import com.rsup.sardjito.model.simetris.dokter.LoginDokter

interface LoginDokterView {
    fun _onSuccess(loginDokter: LoginDokter)
    fun _onFailed()
}