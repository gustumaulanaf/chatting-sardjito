package com.rsup.sardjito.sharedPref

import com.rsup.sardjito.model.simetris.pasien.LoginPasien


class SaveDataPasien {
    internal var loginPasien = LoginPasien()
    fun addDataPasien(new: LoginPasien){
       this.loginPasien = new
    }
    fun getDataPasien(): LoginPasien {
        return loginPasien
    }
}