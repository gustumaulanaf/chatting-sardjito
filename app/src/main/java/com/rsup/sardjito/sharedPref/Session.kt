package com.ic.projectabsensi.sharedPref

class Session private constructor() {

    var isLogin: Boolean
        get() = SharedPrefUtil.getBoolean(IS_LOGIN)
        set(isLogin) = SharedPrefUtil.saveBoolean(IS_LOGIN, isLogin)

    companion object {

        val IS_LOGIN = "IS_LOGIN"

        private var instance: Session? = null

        fun getInstance(): Session {
            if (instance == null) instance = Session()
            return instance as Session
        }
    }
}
