package com.rsup.sardjito.presenter.dokter

import android.content.Context
import com.ic.projectabsensi.sharedPref.SharedPrefUtil
import com.rsup.sardjito.interfaces.dokter.LoginDokterView
import com.rsup.sardjito.model.simetris.dokter.LoginDokter
import com.rsup.sardjito.network.retrofit.BaseUrl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginDokterPresenter (var loginDokterView: LoginDokterView ,var context: Context) {
var baseUrl : BaseUrl? = null
var loginDokter:LoginDokter? = null
    init {
        if (this.baseUrl == null){
            this.baseUrl = BaseUrl(context)
        }

    }
    open fun login(handkey:String){
        baseUrl!!.api.getLoginDokter(handkey).enqueue(object :Callback<LoginDokter>{
            override fun onFailure(call: Call<LoginDokter>?, t: Throwable?) {
                loginDokterView._onFailed()
            }

            override fun onResponse(call: Call<LoginDokter>?, response: Response<LoginDokter>?) {
                if (response!!.isSuccessful){
                if (response.body().msg.equals("")){
                    SharedPrefUtil.saveString("handkey",handkey)
                    loginDokter = response.body()
                    loginDokterView._onSuccess(loginDokter!!)
                }
                    else{
                    loginDokterView._onFailed()

                }
                }
            }

        })
    }
}