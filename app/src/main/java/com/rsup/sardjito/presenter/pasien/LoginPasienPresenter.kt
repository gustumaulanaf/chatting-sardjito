package com.rsup.sardjito.presenter.pasien

import android.content.Context
import com.rsup.sardjito.interfaces.pasien.LoginPasienView
import com.rsup.sardjito.model.simetris.pasien.LoginPasien
import com.rsup.sardjito.network.retrofit.BaseUrl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPasienPresenter(var loginPasienView: LoginPasienView ,var context: Context){
var baseUrl:BaseUrl? = null
    var loginPasien: LoginPasien? = null
    init {
        if (this.baseUrl==null){
            baseUrl = BaseUrl(context)
        }
    }
    open fun getLogin(norm:String ,ttl:String){
        baseUrl!!.api.getLogin(norm,ttl).enqueue(object : Callback<LoginPasien>{
            override fun onFailure(call: Call<LoginPasien>, t: Throwable) {
                loginPasienView.onFailed()
            }

            override fun onResponse(call: Call<LoginPasien>, response: Response<LoginPasien>) {
                if (response.code()==200){
                    if (response.body().msg.equals("")){
                        loginPasien = response.body()
                        loginPasienView.onSuccess(loginPasien!!)
                    }
                    else{
                        loginPasienView.onFailed()
                    }
                }
                else{
                    loginPasienView.onFailed()
                }
            }

        })
    }
}