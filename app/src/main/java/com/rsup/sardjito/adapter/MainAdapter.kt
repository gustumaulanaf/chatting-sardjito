package com.rsup.sardjito.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rsup.sardjito.R
import com.rsup.sardjito.model.firebase.Grup
import com.rsup.sardjito.view.activity.ChatGrupActivity
import kotlinx.android.synthetic.main.item_list_grup.view.*
import java.util.*
import kotlin.collections.ArrayList

class MainAdapter(
    var listGrup: ArrayList<String>,
    var context: Context,
    var listkey: ArrayList<String>
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view :View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_grup,parent!!,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (listGrup!=null){
            return  listGrup.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val grup = listGrup.get(position)
        holder.nama.setText(grup)
        holder.hurufPertama.setText(grup.substring(0,1))
        val random = Random()
        val color = Color.argb(255,random.nextInt(256),random.nextInt(256),random.nextInt(256))
        holder.cardCircle.setCardBackgroundColor(color)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nama = itemView.namaGrup
        var layoutItem = itemView.layoutItemGrup
        var hurufPertama = itemView.hurufPertamaGrup
        var cardCircle = itemView.cardCircleItemGrup
    }
}