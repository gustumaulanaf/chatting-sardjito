package com.rsup.sardjito.network.api

import com.rsup.sardjito.model.simetris.dokter.LoginDokter
import com.rsup.sardjito.model.simetris.pasien.LoginPasien
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface API {
@FormUrlEncoded
@POST("cdp/get_info")
fun getLogin(
    @Field("norm") norm:String,
    @Field("tgl_lahir") tgl_lahir:String
):retrofit2.Call<LoginPasien>
    @FormUrlEncoded
    @POST("cdp/get_dokter_poli")
fun getLoginDokter(
    @Field("handkey") handkey:String
) :Call<LoginDokter>
}