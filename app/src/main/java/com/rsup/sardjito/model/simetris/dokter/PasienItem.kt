package com.rsup.sardjito.model.simetris.dokter

import com.google.gson.annotations.SerializedName

data class PasienItem(

	@field:SerializedName("NAMA")
	val nAMA: String? = null,

	@field:SerializedName("POLI")
	val pOLI: String? = null,

	@field:SerializedName("KODE_POLI")
	val kODEPOLI: String? = null
)