package com.rsup.sardjito.model.simetris.pasien

import com.google.gson.annotations.SerializedName

data class Pasien(

	@field:SerializedName("TELP")
	val tELP: String? = null,

	@field:SerializedName("ALAMAT")
	val aLAMAT: String? = null,

	@field:SerializedName("NAMA")
	val nAMA: String? = null
)