package com.rsup.sardjito.model.simetris.pasien

import com.google.gson.annotations.SerializedName

data class KunjunganItem(

	@field:SerializedName("TOTAL_KUNJUNGAN")
	val tOTALKUNJUNGAN: String? = null,

	@field:SerializedName("POLIKLINIK")
	val pOLIKLINIK: String? = null,

	@field:SerializedName("KODE_POLIKLINIK")
	val kODEPOLIKLINIK: String? = null
)