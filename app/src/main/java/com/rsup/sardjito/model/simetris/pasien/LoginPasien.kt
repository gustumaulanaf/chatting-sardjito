package com.rsup.sardjito.model.simetris.pasien

import com.google.gson.annotations.SerializedName

data class LoginPasien(

    @field:SerializedName("msg")
	val msg: String? = null,

    @field:SerializedName("kunjungan")
	val kunjungan: List<KunjunganItem?>? = null,

    @field:SerializedName("pasien")
	val pasien: Pasien? = null,

    @field:SerializedName("status")
	val status: Int? = null
)