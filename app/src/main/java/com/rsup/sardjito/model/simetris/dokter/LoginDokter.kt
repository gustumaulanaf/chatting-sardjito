package com.rsup.sardjito.model.simetris.dokter

	import com.google.gson.annotations.SerializedName

data class LoginDokter(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("pasien")
	val pasien: List<PasienItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)